#Simulating a Jackal in Gazebo

**Tasks to achieve**

1. Build a new world
2. Launch the world and Jackal
3. Jackal should navigate to the nearest wall and rotate to any random drection (Left/Right)
4. Follow the wall smoothly
5. When a tele-op interrupt is observed, pause the wall-following and start again

### Team

>Deepak Reddy(Team Lead)

>Sam Kysar

>Om Kukunuru


## Working of Project

A launch file ***lab2.launch*** is created which initializes the world ***lab2_jackal.world***, the wall following python file ***laser_scan.py*** and the jackal's movement control file ***twistBuffer.py***

To run the launch file:
***roslaunch lab2_pkg lab2.launch config:=front_laser***

When the launch file is launched, the gazebo brings up the world and jackal is launched. Along with this, jackal navigates to the nearest wall and rotates perpendicular to the wall and starts following it. For the navigation the jackal uses the laser scan data (range and angle) to navigate. To control the jackal's movement, tele-op's functionality is modified

To interrupt the navigation:
***rosrun lab2_pkg key_publisher.py***

A file ***key_publisher.py*** is executed to pause the navigation of jackal. Once the navigation is paused for 10secs, the jackal automatically approches the nearest wall and continue wall following. In the mean time, the jackal can be moved to elsewhere if required using the w,a,s,d,x keys.

The results are present in the ***ScreenGrabs*** folder. A video and an rqt graph is displayed. 

Refer to ***Wiki*** for more details.
